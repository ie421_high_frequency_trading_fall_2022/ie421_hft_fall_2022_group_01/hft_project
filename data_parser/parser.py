import json

import pandas as pd


def parse_json_to_df(json_file_path):
    df = pd.DataFrame()
    with open(json_file_path) as f:
        for line in f:
            df = pd.concat([df, pd.DataFrame(json.loads(line))])

    # unwrap the dictionary in the 'trades' column
    df = df['trades'].apply(pd.Series)

    # keeping only 't' and 'p' columns, which represent timestamp and price
    df = df[['t', 'p']]
    df = df.rename(columns={'t': 'timestamp', 'p': 'price'})

    return df


def parse_parquet(parquet_path):
    df = pd.read_parquet(parquet_path)
    rows = []
    for row in df.iterrows():
        line = json.loads(row[1]["trades"].decode('utf-8'))
        rows.append(line)
    result = pd.DataFrame.from_dict(rows)
    return result


def parse_parquet_you(parquet_path):
    df = pd.read_parquet(parquet_path)
    rows = []
    for row in df.iterrows():
        line = json.loads(row[1]["trades"].decode('utf-8'))
        rows.append(line)
    result = pd.DataFrame.from_dict(rows)
    return result


# for simple testing purposes
if __name__ == '__main__':
    # print(parse_json_to_df('../data_collectors/data/USD102501100-10251600/2022-10-25T11:00:00Z_2022-10-25T16:00'
    #     ':00Z_ERSX_ETHUSD.json'))
    # file_path = "../data_collectors/data/102501100-10251600/ETC/2022-10-25T11:00:00Z_2022-10-25T16:00:00Z_FTXU_ETCUSD.json"
    #  print(parse_json_to_df(file_path).head(100))

    file_path = "/Users/gary/hft_project/data_collectors/data/2022/BTC/CBSE/2022-10-01T00:00:00Z_2022-10-01T23:00:00Z.parquet"
    # print(parse_parquet(file_path).head(100))
    print(pd.read_parquet(file_path))
