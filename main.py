import matplotlib.pyplot as plt

from data_parser import parser

if __name__ == '__main__':
    file_path = "final_data/ETH/2022-01-01T00:00:00Z_2022-11-01T16:00:00Z_CBSE_ETHUSD.parpuet"
    file_path1 = "final_data/ETH/2022-01-01T00:00:00Z_2022-11-01T16:00:00Z_ERSX_ETHUSD.parpuet"
    file_path2 = "final_data/ETH/2022-01-01T00:00:00Z_2022-11-01T16:00:00Z_FTXU_ETHUSD.parpuet"

    df = parser.parse_parquet(file_path)
    df1 = parser.parse_parquet(file_path1)
    df2 = parser.parse_parquet(file_path2)

    # plot df
    plt.plot(df, label='FTXU')
    plt.plot(df1, label='CBSE')
    plt.plot(df2, label='ERSX')
    plt.show()
