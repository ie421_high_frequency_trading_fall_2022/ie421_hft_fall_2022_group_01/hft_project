from pathlib import Path
from typing import Optional, Union, Tuple

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from lead_lag_frameworks.bachelier_model.contrast import ContrastRelation


class LeadLag:

    def __init__(self, input1: pd.Series, input2: pd.Series, lead_lag: float):
        input1.sort_index(inplace=True)
        input2.sort_index(inplace=True)

        self.contrasts = None
        self.contrasts = None
        self.precision = 1
        degree = 2.2
        ts1 = input1.index.values.astype(np.int64) // 10 ** degree
        ts2 = input2.index.values.astype(np.int64) // 10 ** degree
        a_ts1 = np.stack([ts1, input1.values], axis=1)
        a_ts2 = np.stack([ts2, input2.values], axis=1)
        self.x, self.y, self.tx, self.ty, = estimate_value(a_ts1, a_ts2)
        assert len(self.x) == len(self.y)
        lead_lag = int(lead_lag / self.precision)
        if lead_lag <= 0:
            raise Exception('Lead_Lag input Error.')
        self.min_max = np.arange(-lead_lag, lead_lag + 1, 1)
        self.contrasts_relation = ContrastRelation(self.x, self.y, self.tx, self.ty, self.min_max)
        self.contrasts = self.contrasts_relation.shift()

    def lead_lag(self):
        if self.contrasts is None or np.std(self.contrasts) == 0.0:
            return None
        if self.contrasts is not None:
            return self.min_max[np.argmax(self.contrasts)] * self.precision
        else:
            return None

    def leadlag_rate(self):
        if self.contrasts is None:
            return None
        p_i = self.min_max > 0
        n_i = self.min_max < 0
        contrasts_p = np.sum(self.contrasts[p_i])
        contrasts_n = np.sum(self.contrasts[n_i])
        if contrasts_n != 0.0:
            lead_lag_rate = contrasts_p / contrasts_n
        else:
            lead_lag_rate = np.nan
        return lead_lag_rate

    def contrasts_df(self):
        min_max_precise = self.min_max * self.precision
        df = pd.DataFrame(data=np.transpose([min_max_precise, self.contrasts]), columns=['LagRange', 'Contrast'])
        df.set_index('LagRange', inplace=True)
        return df

    def plot_results(self):
        if self.contrasts is not None:
            self.contrasts_df().plot(
                xlabel='Lag (s)',
                ylabel='Contrast (abs cross correlation).'
            )
            plt.show()

def estimate_value(a_ts1: np.array, a_ts2: np.array):
    assert len(a_ts1.shape) == 2
    assert len(a_ts2.shape) == 2
    min_len = min(a_ts1[0, 0], a_ts2[0, 0])
    a_ts1[:, 0] -= min_len
    a_ts2[:, 0] -= min_len
    max_len = int(max(a_ts2[-1, 0], a_ts1[-1, 0]))
    x = np.zeros(shape=max_len + 1) * np.nan
    tx = []
    for i in a_ts1:
        x[int(i[0])] = i[1]
        tx.append(int(i[0]))
    y = np.zeros(shape=max_len + 1) * np.nan
    ty = []
    for j in a_ts2:
        y[int(j[0])] = j[1]
        ty.append(int(j[0]))
    return x, y, tx, ty
