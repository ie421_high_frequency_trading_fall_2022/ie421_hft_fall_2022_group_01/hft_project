import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from lead_lag_frameworks.bachelier_model import lead_lag

def main():
    x_with_ts, y_with_ts, true_lead_lag = bachelier_data()
    ll = lead_lag.LeadLag(x_with_ts, y_with_ts, max_lag=0.0)
    print('PASS!')

if __name__ == '__main__':
    main()