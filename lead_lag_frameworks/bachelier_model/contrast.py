from bisect import bisect_left
import numpy as np
import pandas as pd


class ContrastRelation:

    def __init__(self, x, y, tx, ty, min_max):
        self.x = np.array(x)
        self.y = np.array(y)
        self.tx = np.array(tx)
        self.ty = np.array(ty)
        self.min_max = min_max

    def shift(self):
        contrasts = []
        for i in self.min_max:
            value = manage_time(self.x, self.y, self.tx, self.ty, i)
            contrasts.append(value)
        return np.array(contrasts)

def manage_time(x: np.array, y: np.array,
                tx: np.array, ty: np.array,
                                  i: int):
    result = 0.0
    clips = np.clip(np.array(ty) - i, int(np.min(ty)), int(np.max(ty)))
    for j in zip(tx, tx[1:]):
        i_0 = j[0]
        i_1 = j[1]
        diff = x[i_1] - x[i_0]
        pivot = bisect_left(clips, i_0)
        if pivot is not None:
            mid = pivot
            while True:
                if mid + 1 > len(ty) - 1 or mid < 0:
                    break
                j0, j1 = (ty[mid], ty[mid + 1])
                if overlap(i_0, i_1, j0 - i, j1 - i):
                    result += (y[j1] - y[j0]) * diff
                    mid += 1
                else:
                    break
            mid = pivot - 1
            while True:
                if mid + 1 > len(ty) - 1 or mid < 0:
                    break
                k0, k1 = (ty[mid], ty[mid + 1])
                if overlap(i_0, i_1, k0 - i, k1 - i):
                    result += (y[k1] - y[k0]) * diff
                    mid -= 1
                else:
                    break
        else:
            raise Exception('Time manage Error.')

    return np.abs(result)


def overlap(front: int, end: int, second_front: int, second_end: int) -> bool:
    return min(end, second_end) - max(front, second_front) >= 0
