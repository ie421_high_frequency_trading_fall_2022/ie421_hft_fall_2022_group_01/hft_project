import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from statsmodels.tsa.stattools import grangercausalitytests


# wrapping a class around the granger causality function
class GrangerCausality:
    def __init__(self, df1, df2, maxlag):
        self.df1 = df1
        self.df2 = df2
        self.maxlag = maxlag
        self.F_list = []
        self.max_result = 0

    def granger_causality(self):
        self.F_list, self.max_result = max_granger_causing_lag(self.df1, self.df2, self.maxlag)
        return self.max_result


def max_granger_causing_lag(df1, df2, maxlag=10):
    """
    Returns the maximum F statistic of the second column Granger causing the first column.
    """
    df = pd.concat([df1, df2], axis=1)
    df_reversed = pd.concat([df2, df1], axis=1)

    # using ssr based F test, find lag with largest F statistic

    results = grangercausalitytests(df, maxlag=maxlag, verbose=False)
    reversed_results = grangercausalitytests(df_reversed, maxlag=maxlag, verbose=False)

    max_F = 0
    max_result_lag = 0
    F_list = [0.0] * (2 * maxlag + 1)
    for lag in range(1, maxlag + 1):
        F = results[lag][0]['ssr_ftest'][0]
        reversed_F = reversed_results[lag][0]['ssr_ftest'][0]

        F_list[lag + maxlag - 1] = F
        F_list[maxlag - lag] = reversed_F
        if F > max_F:
            max_F = F
            max_result_lag = lag
        if reversed_F > max_F:
            max_F = reversed_F
            max_result_lag = -lag

    return F_list, max_result_lag


maxlag = 10

if __name__ == '__main__':
    df = pd.read_csv('../data_generator/data.csv')

    df = df.iloc[maxlag:-maxlag, :]
    df_lead_vs_normal = df[['running_lead', 'running_sum']]
    df_lag_vs_normal = df[['running_lag', 'running_sum']]

    #   The data for testing whether the time series in the second column Granger
    #   causes the time series in the first column.

    F_list, lag = max_granger_causing_lag(df['running_lead'], df['running_sum'], maxlag=maxlag)
    F_list2, lag2 = max_granger_causing_lag(df['running_lag'], df['running_sum'], maxlag=maxlag)
    # print(F_list, F_list2)
    plt.plot(F_list, label='lead')
    plt.plot(F_list2, label='lag')
    plt.legend()
    plt.xticks(np.arange(0, 2 * maxlag, 1), np.arange(-maxlag, maxlag, 1))
    plt.xlabel('Lead/Lag')
    plt.ylabel('F statistics')
    plt.title('F statistics result')
    plt.savefig('../data_generator/results/granger_causality.png')

    # logging the results
    with open('../data_generator/results/prediction.txt', 'a+') as f:
        f.write('granger_causality: ' + 'optimal lead=' + str(lag) + ' ' + 'optimal lag=' + str(
            lag2) + '\n')
