# Referenced this paper https://arxiv.org/pdf/2002.00724.pdf
import numpy as np


class NaplesModel:
    def __init__(self, X, Y, t):
        self.X = X
        self.Y = Y
        # t denotes end time, in this case is typically the last time of the dataframe
        self.t = t

    def predict(self):
        return calculate_naples(self.X, self.Y, self.t)


def calculate_naples(X, Y, t):
    # Extract time series data from dataframes
    X_time = X['t'].values
    Y_time = Y['t'].values
    X_price = X['p'].values
    Y_price = Y['p'].values

    # logarithmic returns, denoted by R in paper, i needs to start in 1!!!
    r_X = [np.log(X_price[i] / X_price[i - 1]) for i in range(1, len(X_time))]
    r_Y = [np.log(Y_price[i] / Y_price[i - 1]) for i in range(1, len(Y_time))]

    # signs of returns for X and Y
    b_X = [np.sign(r) for r in r_X]
    b_Y = [np.sign(r) for r in r_Y]

    # cumulative sums of signs of returns for X and Y
    X_cumsum = {}
    X_currsum = 0
    for i in range(1, len(X_time)):
        X_currsum += b_X[i - 1]
        X_cumsum[X_time[i]] = X_currsum

    Y_cumsum = {}
    Y_currsum = 0
    for i in range(1, len(Y_time)):
        Y_currsum += b_Y[i - 1]
        Y_cumsum[Y_time[i]] = Y_currsum
    # print(Y_cumsum)
    estimator = 0
    # print(len(b_X))
    # print(len(Y_cumsum))
    # print(len(X_time))

    i = 1
    j = 1
    # Calculate first term of R(t)
    while i < len(X_time) - 1 and X_time[i + 1] < t:
        while j < len(Y_time) - 1 and Y_time[j] < X_time[i]:
            j += 1
        y_index1 = j
        while j < len(Y_time) - 1 and Y_time[j] < X_time[i + 1]:
            j += 1
        y_index2 = j
        estimator += (b_X[i] * (Y_cumsum[Y_time[y_index2]] - Y_cumsum[Y_time[y_index1]]))

        i += 1

    i = 1
    j = 1
    # Calculate second term of R(t)
    while i < len(Y_time) - 1 and Y_time[i + 1] < t:
        while j < len(X_time) - 1 and X_time[j] < Y_time[i]:
            j += 1
        x_index1 = j
        while j < len(X_time) - 1 and X_time[j] < Y_time[i + 1]:
            j += 1
        x_index2 = j
        estimator -= (b_Y[i] * (X_cumsum[X_time[x_index2]] - X_cumsum[X_time[x_index1]]))

        i += 1

    return estimator
