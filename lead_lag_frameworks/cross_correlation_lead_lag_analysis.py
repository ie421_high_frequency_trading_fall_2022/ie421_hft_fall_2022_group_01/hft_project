import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


# wrapping a class around the cross correlation function
class CrossCorrelation:
    def __init__(self, df1, df2, maxlag):
        self.df1 = df1
        self.df2 = df2
        self.maxlag = maxlag
        self.correlation = []

    def cross_correlation(self):
        for i in range(-self.maxlag, self.maxlag + 1):
            self.correlation.append(self.df.corr(self.df2.shift(i)))
        optimal_lead_index = np.argmax(lead_list_corr) - self.maxlag
        return optimal_lead_index


# using for testing generate
if __name__ == '__main__':
    maxlag = 10

    with open('../data_generator/optimal_lead_lag_data.txt', 'r') as f:
        optimal_lead, optimal_lag = f.read().split()
        optimal_lead = int(optimal_lead)
        optimal_lag = int(optimal_lag)

    df = pd.read_csv('../data_generator/data.csv')
    df = df.iloc[maxlag:-maxlag, :]

    lead_list_corr = []
    lag_list_corr = []
    for i in range(-maxlag, maxlag + 1):
        lead_list_corr.append(df['lead'].corr(df['data'].shift(i)))
        lag_list_corr.append(df['lag'].corr(df['data'].shift(i)))

    # plot lead_list_corr and lag_list_corr
    # print(lead_list_corr)
    plt.plot(lead_list_corr, label='lead')
    plt.plot(lag_list_corr, label='lag')
    plt.legend()
    plt.xticks(np.arange(0, 2 * maxlag, 1), np.arange(-maxlag, maxlag, 1))
    plt.xlabel('Lead/Lag')
    plt.ylabel('Correlation')
    plt.title('Cross Correlation')
    plt.savefig('../data_generator/results/cross_correlation.png')

    # find the optimal lead and lag
    optimal_lead_index = np.argmax(lead_list_corr) - maxlag
    optimal_lag_index = np.argmax(lag_list_corr) - maxlag

    # logging the results
    with open('../data_generator/results/prediction.txt', '+') as f:
        f.write('cross_correlation: ' + 'optimal lead=' + str(optimal_lead_index) + ' ' + 'optimal lag=' + str(
            optimal_lag_index) + '\n')
