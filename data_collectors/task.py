import calendar
import os

import pandas as pd
from fastparquet import write

from data_collectors.safe_request import safe_request


class Task:

    def __init__(self, start, end, exchange, symbol, folder, executor, mode):
        self.start_time = start
        self.end_time = end
        self.exchange = exchange
        self.symbol = symbol
        self.folder = folder
        self.page_token = None
        self.executor = executor
        self.mode = mode

    def update_token(self, new_token):
        self.page_token = new_token

    def save_parquet(self, df, outputfile):
        if not os.path.isfile(outputfile):
            write(outputfile, df)
        else:
            write(outputfile, df, append=True)

    def get_and_load(self, info_dic):
        base_url = info_dic["BASE_URL"]
        headers = {
            'APCA-API-KEY-ID': info_dic["API_KEY"],
            'APCA-API-SECRET-KEY': info_dic["API_SECRET_KEY"]}

        querystring = {"start": self.start_time, "end": self.end_time, "exchanges": self.exchange}
        if self.page_token:
            querystring["page_token"] = self.page_token

        resp = safe_request(f"{base_url}/{self.symbol}/trades", headers=headers,
                            params=querystring, retry_num=self.executor.retry_times)

        # getting the year, month and date from the start time
        year = self.start_time[:4]
        month = calendar.month_name[int(self.start_time[5:7])]
        day = self.start_time[8:10]

        if resp:
            if self.mode == "exchange":
                folder_name = f"{self.executor.folder_path}/{self.folder}/{year}/{month}/{day}"
                isExist = os.path.exists(folder_name)

                if not isExist:
                    os.makedirs(folder_name)
                filename = f"{folder_name}/{self.exchange}.parquet"

            elif self.mode == "crypto":
                folder_name = f"{self.executor.folder_path}/{self.exchange}/{year}/{month}/{day}"
                isExist = os.path.exists(folder_name)
                if not isExist:
                    os.makedirs(folder_name)

                filename = f"{folder_name}/{self.folder}.parquet"

            json_data = resp.json()
            next_token = json_data["next_page_token"]

            if json_data["trades"] is not None:
                df = pd.DataFrame.from_dict(json_data["trades"])
                self.save_parquet(df, filename)

            return next_token

        return None
