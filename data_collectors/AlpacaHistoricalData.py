import json
import os
import time
from calendar import monthrange

import requests

from data_collectors.task import Task
from data_collectors.task_executor import Executor


def parse(start_datetime_string, end_datetime_string, exchange, symbol):
    print("Running")
    base_url = "https://data.alpaca.markets/v1beta1/crypto"

    APCA_API_KEY_ID = None  # enter key id here
    APCA_API_SECRET_KEY = None  # enter secret key here

    headers = {
        'APCA-API-KEY-ID': APCA_API_KEY_ID,
        'APCA-API-SECRET-KEY': APCA_API_SECRET_KEY}

    querystring = {"start": start_datetime_string, "end": end_datetime_string, "limit": 10, "exchanges": exchange}

    resp = requests.get(f"{base_url}/{symbol}/trades", headers=headers, params=querystring)
    print(f"API Response Status: {resp.status_code}")

    filename = "data/" + start_datetime_string + "_" + end_datetime_string + "_" + exchange + "_" + symbol + ".json"

    json_data = resp.json()
    json_file = open(filename, "w+")
    json.dump(json_data, json_file)


# collects daily data for a month
def collect_daily_data(year, month, exchanges, cryptos,symbols, exec, mode):
    for day in range(1,  monthrange(year, int(month))[1]+1):
        if day < 10:
            day = "0" + str(day)
        start_datetime_string = f"{year}-{month}-{day}T00:00:00Z"
        end_datetime_string = f"{year}-{month}-{day}T23:59:59Z"
        if mode == "exchange":
            for exchange in exchanges:
                task = Task(start_datetime_string, end_datetime_string, exchange, cryptos[0], symbols[0], exec, mode)
                exec.addTask(task)
        elif mode == "crypto":
            for crypto, symbol in zip(cryptos, symbols):
                task = Task(start_datetime_string, end_datetime_string, exchanges[0], crypto, symbol, exec, mode)
                exec.addTask(task)

def collect(year, month, exchanges, crypto, symbol, mode):
    # mode = "exchange"  # "exchange" or "crypto"
    folder_path = f"data_collectors/data/{mode}"

    if not os.path.exists(folder_path):
        os.makedirs(folder_path)
    retry_times = 10
    executor = Executor(folder_path, retry_times)

    collect_daily_data(year, month, exchanges, crypto, symbol, executor, mode)
    # collect_daily_data(2022, "01", ["FTXU", "ERSX"], "UNIUSD", "UNI", executor, mode)
    # collect_daily_data(2022, "01", ["FTXU", "ERSX"], "ETHUSD", "ETH", executor, mode)
    # collect_daily_data(2022, "01", ["FTXU", "ERSX"], "LTCUSD", "LTC", executor, mode)
    print("Begin")

    cur = 0
    while len(executor.queue) > 0:
        print('\rdownloading : ' + str(cur), end='')
        executor.execute_task()
        cur += 1

    print()
    print("Done!")


if __name__ == '__main__':
    # collect(2021, "12", ['FTXU', 'CBSE', 'ERSX'], ["BTCUSD"], ["BTC"], "crypto")
    collect(2021, "12", ['FTXU'], ["BTCUSD", "UNIUSD"], ["BTC", "UNI"], "crypto")