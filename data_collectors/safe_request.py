import time

import requests


def safe_request(url, headers, params, retry_num):
    for i in range(retry_num):
        resp = requests.get(url, headers=headers, params=params)
        if resp.status_code == 200:
            return resp
        else:
            print("Bad request code:", resp.status_code)
            print("Number:", i, "Retry of Total:", retry_num)
            time.sleep(63)
    return None
