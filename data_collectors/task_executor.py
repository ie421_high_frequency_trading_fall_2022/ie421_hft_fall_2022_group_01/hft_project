from data_collectors.task import Task
import collections

class Executor:
    def __init__(self, folder_path, retry_times):
        self.queue = collections.deque()
        self.folder_path = folder_path
        self.API_KEY = None # CHANGE THIS WHEN NEEDED
        self.API_SECRET_KEY = None # CHANGE THIS WHEN NEEDED
        self.info_dic = {
            "API_KEY": self.API_KEY,
            "API_SECRET_KEY": self.API_SECRET_KEY,
            "BASE_URL": "https://data.alpaca.markets/v1beta1/crypto"
        }
        self.retry_times = retry_times

    def addTask(self, task: Task):
        self.queue.append(task)

    def execute_task(self):
        task = self.queue.popleft()
        next_token = task.get_and_load(self.info_dic)

        if next_token:
            task.update_token(next_token)
            self.addTask(task)
        return
