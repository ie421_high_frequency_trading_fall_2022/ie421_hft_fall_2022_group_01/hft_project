import random

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

if __name__ == '__main__':
    scale = 10
    noise_scale = 2

    # Generate a white noise time series data
    data = scale * np.random.randn(1000, 1)

    optimal_lead = -np.random.randint(1, 10)
    optimal_lag = np.random.randint(1, 10)

    # Create a pandas dataframe
    df = pd.DataFrame(data, columns=['data'])
    # Add an integer random lead and lag to the data
    df['lead'] = df['data'].shift(optimal_lead)
    df['lag'] = df['data'].shift(optimal_lag)

    # Add noise to lead and lag data
    print(df['lead'].shape)
    df['lead'] = df['lead'] + noise_scale * np.random.randn(1000, 1).reshape(1000)
    df['lag'] = df['lag'] + noise_scale * np.random.randn(1000, 1).reshape(1000)

    # Add outliers to lead and lag data, only 3% chance of happening
    df['lead'] = df['lead'].mask(np.random.rand(1000) > 0.97, np.random.randint(4, 10) * np.random.randn(1000) +
                                 np.random.rand(1000))
    df['lag'] = df['lag'].mask(np.random.rand(1000) > 0.97, np.random.randint(4, 10) * np.random.randn(1000) +
                               np.random.rand(1000))

    # running sum of the data
    df['running_sum'] = df['data'].cumsum()
    df['running_lead'] = df['lead'].cumsum()
    df['running_lag'] = df['lag'].cumsum()

    df.to_csv('data.csv', index=False)
    # Put optimal lead and lag in a file
    with open('optimal_lead_lag_data.txt', 'w') as f:
        f.write(str(optimal_lead) + ' ' + str(optimal_lag))

    # Visualization
    plt.plot(df['running_sum'], label='data')
    plt.plot(df['running_lead'], label='lead')
    plt.plot(df['running_lag'], label='lag')
    plt.legend()

    plt.xlabel('Time')
    plt.ylabel('Data')
    plt.title('Time Series Data')

    #plt.show()
    plt.savefig('data.png')
