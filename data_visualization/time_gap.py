import pandas as pd
from datetime import datetime
import matplotlib.pyplot as plt
from matplotlib.dates import DateFormatter, date2num

s = '2022-10-01T00:05:54.4989'
print(datetime.strptime(s, '%Y-%m-%dT%H:%M:%S.%f'))


file_path = "/Users/Lyuyou/PycharmProjects/hft_project/data_collectors/data/2022/BTC/2022-10-01T00:00:00Z_2022-10-01T23:00:00Z_ERSX_BTCUSD.parpuet"
#print(parse_parquet(file_path).head(100))


df = pd.read_parquet(file_path)

gap = []
last_time = None
for index, row in df.iterrows():
    print(row['t'])
    if not last_time:
        last_time = datetime.strptime(row["t"][:-4], '%Y-%m-%dT%H:%M:%S.%f')
    cur = datetime.strptime(row["t"][:-4], '%Y-%m-%dT%H:%M:%S.%f')
    gap.append((cur - last_time).total_seconds())
    last_time = cur


df = pd.DataFrame({'Index':[ i for i in range(len(gap))],
                   'Gap': gap})

fig, ax = plt.subplots()

# myFmt = DateFormatter("%H:%M:%S")
# ax.yaxis.set_major_formatter(myFmt)

ax.plot(df['Index'], df['Gap'])

plt.gcf().autofmt_xdate()
plt.xlabel("index")
plt.ylabel("gap in seconds")
plt.show()