import matplotlib.pyplot as plt

from data_analyzer.bachelier_analyzer import analyze_monthly_across_cryptos
from data_analyzer.bachelier_analyzer import analyze_monthly_across_exchanges



# referenced: https://arxiv.org/pdf/2002.00724.pdf
def plot_analysis(bachelier_dict):
    # change this part later to be more dynamic
    #keys = ["CBSE vs ERSX", "CBSE vs FTXU", "ERSX vs FTXU"]
    keys = bachelier_dict.keys()#["ETH vs LTC", "BTC vs ETH", "BTC vs LTC"]

    for key in keys:
        plot(bachelier_dict[key], key)


def plot(values, key):
    # distribution of the data
    plt.figure(figsize=(10, 10))
    plt.ylim(min(values) - 10, max(values) + 10)
    val1, val2 = key.split(" vs ")
    plt.title(f"{val1} vs {val2}")
    plt.xlabel("Time(Days)")
    plt.ylabel(f"(<--{val1} is faster if negative)  Lead-Lag Analysis(ns) ({val2} is faster if positive -->)")
    plt.scatter(range(len(values)), values, c='black')
    plt.show()

    # box plot of the data
    plt.figure(figsize=(10, 10))
    plt.title(f"{val1} vs {val2}")
    plt.ylabel(f"(<--{val1} is faster if negative)  Lead-Lag Analysis(ns) ({val2} is faster if positive -->)")
    plt.boxplot(values)
    plt.show()

    # distribution of the data
    plt.figure(figsize=(10, 10))
    plt.title(f"{val1} vs {val2}")
    plt.xlabel(f"(<--{val1} is faster if negative)  Lead-Lag Analysis(ns) ({val2} is faster if positive -->)")
    plt.ylabel(f"Count")
    plt.hist(values, bins=20)
    plt.show()

if __name__ == '__main__':
    #plot_analysis(analyze_monthly_across_cryptos(2022, 1, "FTXU", ["BTC", "ETH", "LTC"], "result_crypto.txt", verbose=True))
    plot_analysis(analyze_monthly_across_exchanges(2022, 3, ["CBSE", "ERSX", "FTXU"], "BTC", "result_exchange.txt", verbose=True))