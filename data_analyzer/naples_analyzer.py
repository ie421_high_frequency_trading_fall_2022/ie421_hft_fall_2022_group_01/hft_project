import calendar
import collections
import os
from calendar import monthrange

import pandas as pd

from lead_lag_frameworks import naples_model


def analyze_yearly_data(year, exchanges, bitcoin, file_path, verbose=False, mode="exchange"):
    dic = collections.defaultdict(list)
    for month in range(1, 13):
        if mode == "exchange":
            analyze_monthly_across_exchanges(year, month, exchanges, bitcoin, file_path, verbose, dic)

        elif mode == "crypto":
            analyze_monthly_across_cryptos(year, month, exchanges, bitcoin, file_path, verbose, dic)
    return dic

# analyze monthly crypto data across exchanges
def analyze_monthly_across_exchanges(year, month, exchanges, bitcoin, file_path, verbose=False, dic = None):
    if dic is None:
        dic = collections.defaultdict(list)
    if os.path.exists(file_path):
        os.remove(file_path)
    for day in range(1, monthrange(year, int(month))[1] + 1):
        if day < 10:
            day = "0" + str(day)
        link_array = []
        for exchange in exchanges:
            link_array.append([exchange,
                               f"../data_collectors/data/exchange/{bitcoin}/{year}/{calendar.month_name[month]}/{day}/{exchange}.parquet"])
        # use naples model to analyze all data in link_array
        for i in range(len(link_array)):
            for j in range(i + 1, len(link_array)):
                if verbose:
                    # index 1 is the link
                    print(f"Analyzing {link_array[i][0]} and {link_array[j][0]} of {bitcoin} for {year}-{month}-{day}")
                try:
                    df1 = pd.read_parquet(link_array[i][1])
                except FileNotFoundError:
                    print(f"File not found: {link_array[i][1]}")
                    continue
                try:
                    df2 = pd.read_parquet(link_array[j][1])
                except FileNotFoundError:
                    print(f"File not found: {link_array[j][1]}")
                    continue

                # last time of dataframe
                t1 = df1['t'].iloc[-1]
                t2 = df2['t'].iloc[-1]
                t = max(t1, t2)


                try:
                    result = naples_model.NaplesModel(df1, df2, t).predict()

                    # write result to a file named output
                    with open(file_path, 'a+') as f:
                        f.write(f"{link_array[i][0]} vs {link_array[j][0]}: {result}\n")

                    # write result to dict
                    dic[f"{link_array[i][0]} vs {link_array[j][0]}"].append(result)
                except:
                    print(f"ValueError: {link_array[i][0]} vs {link_array[j][0]}")
    return dic


def analyze_monthly_across_cryptos(year, month, exchange, bitcoins, file_path, verbose=False, dic = None):
    if dic is None:
        dic = collections.defaultdict(list)
    if os.path.exists(file_path):
        os.remove(file_path)
    for day in range(1, monthrange(year, int(month))[1] + 1):
        if day < 10:
            day = "0" + str(day)
        link_array = []
        for bitcoin in bitcoins:
            link_array.append([bitcoin,
                               f"../data_collectors/data/crypto/{exchange}/{year}/{calendar.month_name[month]}/{day}/{bitcoin}.parquet"])
        # use naples model to analyze all data in link_array
        for i in range(len(link_array)):
            for j in range(i + 1, len(link_array)):
                if verbose:
                    # index 1 is the link
                    print(f"Analyzing {link_array[i][0]} and {link_array[j][0]} of {exchange} for {year}-{month}-{day}")
                try:
                    df1 = pd.read_parquet(link_array[i][1])
                except FileNotFoundError:
                    print(f"File not found: {link_array[i][1]}")
                    continue
                try:
                    df2 = pd.read_parquet(link_array[j][1])
                except FileNotFoundError:
                    print(f"File not found: {link_array[j][1]}")
                    continue

                # last time of dataframe
                t1 = df1['t'].iloc[-1]
                t2 = df2['t'].iloc[-1]
                t = max(t1, t2)

                try:
                    result = naples_model.NaplesModel(df1, df2, t).predict()

                    # write result to a file named output
                    with open(file_path, 'a+') as f:
                        f.write(f"{link_array[i][0]} vs {link_array[j][0]}: {result}\n")

                    # write result to dict
                    dic[f"{link_array[i][0]} vs {link_array[j][0]}"].append(result)
                except:
                    print(f"ValueError: {link_array[i][0]} vs {link_array[j][0]}")
    return dic


if __name__ == '__main__':
   # analyze_monthly_across_exchanges(2022, 1, ["CBSE", "ERSX", "FTXU"], "BTC", "result_exchange.txt", verbose=True)

    analyze_monthly_across_cryptos(2022, 1, "FTXU", ["BTC", "ETH", "LTC"], "result_crypto.txt", verbose=True)
