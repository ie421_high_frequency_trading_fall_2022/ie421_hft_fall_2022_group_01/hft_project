import calendar
import collections
import os
from datetime import datetime
import time
from calendar import monthrange

import pandas as pd
import numpy as np

from lead_lag_frameworks.bachelier_model import lead_lag


def analyze_data_by_year(year, exchanges, bitcoin, file_path, verbose=False):
    for month in range(1, 13):
        analyze_monthly_across_exchanges(year, month, exchanges, bitcoin, file_path, verbose)


def analyze_monthly_across_exchanges(year, month, exchanges, bitcoin, file_path, verbose=False):
    dic = collections.defaultdict(list)
    if os.path.exists(file_path):
        os.remove(file_path)
    for day in range(1, monthrange(year, int(month))[1] + 1):
        if day < 10:
            day = "0" + str(day)
        link_array = []
        for exchange in exchanges:
            link_array.append([exchange,
                               f"data_collectors/data/exchange/{bitcoin}/{year}/{calendar.month_name[month]}/{day}/{exchange}.parquet"])
        # use bachelier model to analyze all data in link_array
        for i in range(len(link_array)):
            for j in range(i + 1, len(link_array)):
                if verbose:
                    # index 1 is the link
                    print(f"Analyzing {link_array[i][0]} and {link_array[j][0]} of {bitcoin} for {year}-{month}-{day}")
                try:
                    df1 = pd.read_parquet(link_array[i][1])
                except FileNotFoundError:
                    print(f"File not found: {link_array[i][1]}")
                    continue
                try:
                    df2 = pd.read_parquet(link_array[j][1])
                except FileNotFoundError:
                    print(f"File not found: {link_array[j][1]}")
                    continue
                df1.dropna(inplace=True)
                df2.dropna(inplace=True)

                X_time = df1['t'].values
                Xt = []
                x_drop = []
                for idx_x in range(0, len(X_time)):
                    date_string = X_time[idx_x]
                    try:
                        date_string = date_string.strip()  # remove any leading or trailing spaces
                        date_string = date_string.split('.')[0] + 'Z'  # truncate the string to match the format
                        datetime_object = datetime.strptime(date_string, "%Y-%m-%dT%H:%M:%SZ")
                        unix_timestamp = time.mktime(datetime_object.timetuple())
                        float_timestamp = float(unix_timestamp)
                        Xt.append(float_timestamp)
                    except:
                        print(idx_x)
                        x_drop.append(idx_x)
                        continue


                Y_time = df2['t'].values
                Yt = []
                y_drop = []
                for idx_y in range(0, len(Y_time)):
                    date_string = Y_time[idx_y]
                    try:
                        date_string = date_string.strip()  # remove any leading or trailing spaces
                        date_string = date_string.split('.')[0] + 'Z'  # truncate the string to match the format
                        datetime_object = datetime.strptime(date_string, "%Y-%m-%dT%H:%M:%SZ")
                        unix_timestamp = time.mktime(datetime_object.timetuple())
                        float_timestamp = float(unix_timestamp)
                        Yt.append(float_timestamp)
                    except:
                        y_drop.append(idx_y)
                        continue


                X_price = df1['p'].values
                Y_price = df2['p'].values

                if len(x_drop):
                    # print(len(x_drop))
                    for i_drop in range(0, len(x_drop)):
                        X_price = np.delete(X_price, i_drop)

                if len(y_drop):
                    for j_drop in range(0, len(y_drop)):
                        Y_price = np.delete(Y_price, j_drop)

                s1 = pd.Series(data=X_price, index=Xt)
                s2 = pd.Series(data=Y_price, index=Yt)

                # result = lead_lag.LeadLag(s1, s2, 3.0).lead_lag()
                # result = lead_lag.LeadLag(s1, s2, 3.0).plot_results()
                result = lead_lag.LeadLag(s1, s2, 3.0).leadlag_rate()

                # write result to a file named output
                # print(i)
                # print(j)
                with open(file_path, 'a+') as f:
                    f.write(f"{link_array[i][0]} vs {link_array[j][0]}: {result}\n")

                # write result to dict
                dic[f"{link_array[i][0]} vs {link_array[j][0]}"].append(result)
    return dic


##############################################################################################################
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
##############################################################################################################

def analyze_monthly_across_cryptos(year, month, exchange, bitcoins, file_path, verbose=False):
    dic = collections.defaultdict(list)
    if os.path.exists(file_path):
        os.remove(file_path)
    for day in range(1, monthrange(year, int(month))[1] + 1):
        if day < 10:
            day = "0" + str(day)
        link_array = []
        for bitcoin in bitcoins:
            link_array.append([bitcoin,
                               f"../data_collectors/data/crypto/{exchange}/{year}/{calendar.month_name[month]}/{day}/{bitcoin}.parquet"])
        # use naples model to analyze all data in link_array
        for i in range(len(link_array)):
            for j in range(i + 1, len(link_array)):
                if verbose:
                    # index 1 is the link
                    print(f"Analyzing {link_array[i][0]} and {link_array[j][0]} of {exchange} for {year}-{month}-{day}")
                try:
                    df1 = pd.read_parquet(link_array[i][1])
                except FileNotFoundError:
                    print(f"File not found: {link_array[i][1]}")
                    continue
                try:
                    df2 = pd.read_parquet(link_array[j][1])
                except FileNotFoundError:
                    print(f"File not found: {link_array[j][1]}")
                    continue

                df1.dropna(inplace=True)
                df2.dropna(inplace=True)

                X_time = df1['t'].values
                Xt = []
                x_drop = []
                for idx_x in range(0, len(X_time)):
                    date_string = X_time[idx_x]
                    try:
                        date_string = date_string.strip()  # remove any leading or trailing spaces
                        date_string = date_string.split('.')[0] + 'Z'  # truncate the string to match the format
                        datetime_object = datetime.strptime(date_string, "%Y-%m-%dT%H:%M:%SZ")
                        unix_timestamp = time.mktime(datetime_object.timetuple())
                        float_timestamp = float(unix_timestamp)
                        Xt.append(float_timestamp)
                    except:
                        print(idx_x)
                        x_drop.append(idx_x)
                        continue

                Y_time = df2['t'].values
                Yt = []
                y_drop = []
                for idx_y in range(0, len(Y_time)):
                    date_string = Y_time[idx_y]
                    try:
                        date_string = date_string.strip()  # remove any leading or trailing spaces
                        date_string = date_string.split('.')[0] + 'Z'  # truncate the string to match the format
                        datetime_object = datetime.strptime(date_string, "%Y-%m-%dT%H:%M:%SZ")
                        unix_timestamp = time.mktime(datetime_object.timetuple())
                        float_timestamp = float(unix_timestamp)
                        Yt.append(float_timestamp)
                    except:
                        y_drop.append(idx_y)
                        continue

                X_price = df1['p'].values
                Y_price = df2['p'].values
                # print(X_price)

                if len(x_drop):
                    print(len(x_drop))
                    for i_drop in range(0, len(x_drop)):
                        X_price = np.delete(X_price, i_drop)

                if len(y_drop):
                    for j_drop in range(0, len(y_drop)):
                        Y_price = np.delete(Y_price, j_drop)

                s1 = pd.Series(data=X_price, index=Xt)
                s2 = pd.Series(data=Y_price, index=Yt)

                #result = lead_lag.LeadLag(s1, s2, 3.0).lead_lag()
                #result = lead_lag.LeadLag(s1, s2, 3.0).plot_results()
                result = lead_lag.LeadLag(s1, s2, 3.0).leadlag_rate()

                # write result to a file named output
                with open(file_path, 'a+') as f:
                    f.write(f"{link_array[i][0]} vs {link_array[j][0]}: {result}\n")

                # write result to dict
                dic[f"{link_array[i][0]} vs {link_array[j][0]}"].append(result)
    return dic

if __name__ == '__main__':
    #analyze_monthly_across_exchanges(2022, 3, ["CBSE", "ERSX", "FTXU"], "BTC", "result_exchange.txt", verbose=True)
    analyze_monthly_across_cryptos(2022, 1, "FTXU", ["BTC", "ETH", "LTC"], "result_crypto.txt", verbose=True)