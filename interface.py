import calendar
import matplotlib.pyplot as plt
import os
import pickle
import json
from calendar import monthrange
from data_collectors.AlpacaHistoricalData import collect

import data_visualization.naples_visualization as naples_visualization
from data_analyzer import naples_analyzer as nap

import data_visualization.bachelier_visualization as bachelier_visualization
from data_analyzer import bachelier_analyzer as bac


from lead_lag_frameworks.bachelier_model.lead_lag import LeadLag

dataLocation = "data_collectors/data"


def ask_autofill():
    enable = input("Missing Data Detected, do you like to download now? (Y/n)")
    return enable == "Y"

def check_mode_selection(model):
    if model not in ["naples", "bachelier"]:
        model = input("choose from the two options: 1: naples; 2: bachelier")
        if model == "1":
            model = "naples"
        elif model == "2":
            model = "bachelier"
        if model not in ["naples", "bachelier"]:
            print("Mode not recognized!")
            return "Bad"
    return model


def check_exchange_ensured(dataLocation, year, month, crypto):
    auto_download = False


    try:
        dataLocation = dataLocation + "/exchange"
        year = str(year)
        checked = True

        # Check crypto folder
        cryptos_db = os.listdir(dataLocation)
        if crypto not in cryptos_db:
            checked = False

        # Check year folder
        year_db = os.listdir(f'{dataLocation}/{crypto}')
        if year not in year_db:
            checked = False

        # Check year folder
        month_db = os.listdir(f'{dataLocation}/{crypto}/{year}')
        if calendar.month_name[month] not in month_db:
            checked = False

        return checked
    except:
        return False


def check_crypto_ensured(dataLocation, exchange, year, month):
    auto_download = False

    try:
        dataLocation = dataLocation + "/crypto"
        year = str(year)
        checked = True

        # Check crypto folder
        exchanges_db = os.listdir(dataLocation)
        if exchange not in exchanges_db:
            checked = False
        else:
            year_db = os.listdir(f'{dataLocation}/{exchange}')
            if year not in year_db:
                checked = False
            month_db = os.listdir(f'{dataLocation}/{exchange}/{year}')
            if calendar.month_name[month] not in month_db:
                checked = False
        return checked
    except:
        return False


def naples(year, month, configjson, file_path, mode):
    if mode == "exchange":
        crypto = configjson[mode]["Symbols"][0]
        if os.path.exists("naples_dict.pickle"):
            with open("naples_dict.pickle", "rb") as infile:
                naples_dict = pickle.load(infile)
        else:
            naples_dict = nap.analyze_monthly_across_exchanges(year, month, configjson[mode]["Exchanges"], crypto,
                                                                           file_path, verbose=True)
            with open("naples_dict.pickle", "wb") as outfile:
                pickle.dump(naples_dict, outfile)
        print(naples_dict)

        naples_visualization.plot_analysis(naples_dict)

    elif mode == "crypto":
        if os.path.exists("naples_dict.pickle"):
            with open("naples_dict.pickle", "rb") as infile:
                naples_dict = pickle.load(infile)
        else:
            naples_dict = nap.analyze_monthly_across_cryptos(year, month, configjson[mode]["Exchanges"][0], configjson[mode]["Symbols"], file_path, verbose=True)
            with open("naples_dict.pickle", "wb") as outfile:
                pickle.dump(naples_dict, outfile)
        print(naples_dict)

        naples_visualization.plot_analysis(naples_dict)


def bachelier(year, month, configjson, file_path, mode):
    if mode == "exchange":
        crypto = configjson[mode]["Symbols"][0]
        if os.path.exists("bachelier_dict.pickle"):
            with open("bachelier_dict.pickle", "rb") as infile:
                bachelier_dict = pickle.load(infile)
        else:
            bachelier_dict = bac.analyze_monthly_across_exchanges(year, month, configjson[mode]["Exchanges"], crypto,
                                                               file_path, verbose=True)
            with open("bachelier_dict.pickle", "wb") as outfile:
                pickle.dump(bachelier_dict, outfile)
        print(bachelier_dict)

        bachelier_visualization.plot_analysis(bachelier_dict)

    elif mode == "crypto":
        if os.path.exists("bachelier_dict.pickle"):
            with open("bachelier_dict.pickle", "rb") as infile:
                bachelier_dict = pickle.load(infile)
        else:
            bachelier_dict = bac.analyze_monthly_across_cryptos(year, month, configjson[mode]["Exchanges"][0],
                                                             configjson[mode]["Symbols"], file_path, verbose=True)
            with open("bachelier_dict.pickle", "wb") as outfile:
                pickle.dump(bachelier_dict, outfile)
        print(bachelier_dict)

        bachelier_visualization.plot_analysis(bachelier_dict)

def start_console():
    with open('config.json') as json_file:
        configjson = json.load(json_file)

        model = configjson["Model"]
        mode = configjson["Mode"]

        model = check_mode_selection(model)
        if model == "Bad":
            exit()

        year = configjson[mode]["Year"]
        month = int(configjson[mode]["Month"])
        file_path = configjson[mode]["OutputPath"]

        checked = True

        if mode == "exchange":
            checked = check_exchange_ensured(dataLocation, year, month, configjson[mode]["Symbols"][0])
            if not checked:
                if ask_autofill():
                    # collect
                    print("collecting")
                    formatted_month = configjson[mode]["Month"]
                    print(configjson[mode]["Cryptos"][0], configjson[mode]["Symbols"][0])
                    collect(year, formatted_month, configjson[mode]["Exchanges"],
                            configjson[mode]["Cryptos"], configjson[mode]["Symbols"], mode)
                else:
                    print("Denied to collect data, task failed")
                    exit(0)
        else:
            checked = check_crypto_ensured(dataLocation, configjson[mode]["Exchanges"][0], year, month)
            if not checked:
                if ask_autofill():
                    # collect
                    print("collecting")
                    formatted_month = configjson[mode]["Month"]
                    collect(year, formatted_month, configjson[mode]["Exchanges"],
                            configjson[mode]["Cryptos"], configjson[mode]["Symbols"], mode)
                else:
                    print("Denied to collect data, task failed")
                    exit(0)

        # TODO: this part needs to let user choose which analyzer to use




        if model == "naples":
            naples(year, month, configjson, file_path, mode)
        elif model == "bachelier":
            bachelier(year, month, configjson, file_path, mode)



if __name__ == '__main__':
    start_console()
